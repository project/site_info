# INTRODUCTION

The Site Info module allows developers to Download a xls file with all the information on the site i.e number of content, content types with their fields, modules, blocks etc. This information is helpful in migrating the site to higher Drupal version and you need to have a in hand document about all the information of the site.

# DEPENDENCIES

This module depends on the PHPExcel module (https://www.drupal.org/project/phpexcel).
And Menu module (a drupal core module).

In order for this module to work, you must have to configure PHPExcel module properly with its library (http://phpexcel.codeplex.com/)

# INSTALLATION

1. Install the module as any other Drupal module. 
2. Make sure you have downloaded PHPExcel library and extract it inside your site's libraries folder as mentioned in PHPExcel module documentation.


